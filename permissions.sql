CREATE ROLE BD_administrator WITH  SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE Sales_manager WITH LOGIN;
CREATE ROLE Site_administrator WITH LOGIN;
CREATE ROLE Delivery_manager WITH LOGIN;
CREATE ROLE Curier WITH LOGIN;
CREATE ROLE Client WITH LOGIN;
CREATE ROLE Guest WITH NOLOGIN;

GRANT SELECT(email, first_name, middle_name, last_name, discount, amount) ON "Customer" TO Sales_manager, Delivery_manager,Site_administrator, Curier;

GRANT CONNECT ON DATABASE "Internet_shop" TO Client;

GRANT SELECT, UPDATE ON "Customer", "Credit_card", "Order", "Order/product" TO Client;

GRANT SELECT, UPDATE ON "Product" TO Sales_manager, Site_administrator;

GRANT SELECT ON "Point_delivery", "Product", "Delivery" TO Guest;

REVOKE INSERT, UPDATE, DELETE ON "Product" FROM Client;

REVOKE INSERT, UPDATE, DELETE, SELECT ON "Customer", "Credit_card", "Customer/phone_number", "Employee", "Order", "Supplier","Supplier/category", "Order/product" FROM Guest;



