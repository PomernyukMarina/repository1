-- �����������, ������������� ��������� ������� ����� ��� ���������� ������ (�.�. �3 ��. 1.3 ��� 1.2)

CREATE TABLE "Product"
(
  product_number integer ,
  price numeric(10,2) ,
  product_name text ,
  category_name text ,
  subcategory_name text 
);

-- ������� ���������, ��� ������ �����, �����, ��������� � ������������ ������ � ��� ���� �������� ������������� ���������.

CREATE FUNCTION prod_stamp() RETURNS trigger AS $prod_stamp$
    BEGIN
    
	-- Check that product_number, price, product_name and category_name are given
	IF NEW.product_name IS NULL THEN
            RAISE EXCEPTION 'product_name cannot be null';
        END IF;
	IF NEW.product_number IS NULL THEN
            RAISE EXCEPTION '% cannot have null product_number', NEW.product_name;
        END IF;
        IF NEW.price IS NULL THEN
            RAISE EXCEPTION '% cannot have null price', NEW.product_name;
        END IF;
        IF NEW.category_name IS NULL THEN
            RAISE EXCEPTION 'category_name cannot be null';
        END IF;
        IF NEW.subcategory_name IS NULL THEN
            RAISE EXCEPTION 'subcategory_name cannot be null';
        END IF;
        -- 
        IF NEW.price < 0 THEN
            RAISE EXCEPTION '% cannot have a negative price', NEW.product_name;
        END IF;

        RETURN NEW;
    END;
$prod_stamp$ LANGUAGE plpgsql;

CREATE TRIGGER prod_stamp BEFORE INSERT OR UPDATE ON "Product"
    FOR EACH ROW EXECUTE PROCEDURE prod_stamp();



-- �������������� ��������� ��� ����� �� ������
    
CREATE TABLE "Order/product"
(
  order_number integer NOT NULL,
  product_name text NOT NULL,
  PRIMARY KEY (order_number),
  FOREIGN KEY (order_number)
      REFERENCES "Order" (order_number),
  FOREIGN KEY (product_name)
      REFERENCES "Product" (product_name)
);

CREATE TABLE "Order/product_audit"
(
    operation         char(1)   NOT NULL,
    stamp             timestamp NOT NULL,
    userid            text      NOT NULL,
    product_name      text      NOT NULL,
    order_number 	    integer
);

CREATE TABLE "Order/product_audit"
(
    operation         char(1)   NOT NULL,
    stamp             timestamp NOT NULL,
    userid            text      NOT NULL,
    product_name      text      NOT NULL,
    order_number      integer
);

CREATE OR REPLACE FUNCTION process_audit() RETURNS TRIGGER AS $audit$
    BEGIN
        
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO "Order/product_audit" SELECT 'D', now(), user, OLD.*;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO "Order/product_audit" SELECT 'U', now(), user, NEW.*;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO "Order/product_audit" SELECT 'I', now(), user, NEW.*;
            RETURN NEW;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$audit$ LANGUAGE plpgsql;

CREATE TRIGGER audit
AFTER INSERT OR UPDATE OR DELETE ON "Order/product"
    FOR EACH ROW EXECUTE PROCEDURE process_audit();

    
 -- ����������� ��������� ����������� (���������� ���� �� �.�. �2)

CREATE TABLE "Product"
(
  product_number integer NOT NULL,
  price numeric(10,2) NOT NULL,
  product_name text NOT NULL,
  category_name text NOT NULL,
  subcategory_name text NOT NULL,
  PRIMARY KEY (product_name),
  CHECK (category_name ~* '^[A-Za-z -]+$'),
  CHECK (product_name ~* '^[A-Za-z -]+$'),
  CHECK (subcategory_name ~* '^[A-Za-z -]+$')
);

CREATE TABLE "Order/product"
(
  order_number integer NOT NULL,
  product_name text NOT NULL,
  PRIMARY KEY (order_number),
  FOREIGN KEY (order_number)
      REFERENCES "Order" (order_number),
  FOREIGN KEY (product_name)
      REFERENCES "Product" (product_name)
);


CREATE OR REPLACE FUNCTION prod_fk() RETURNS TRIGGER AS $prod_fk$
    DECLARE
        tmp          RECORD;
    BEGIN
        
        IF NEW.product_name  IS NOT NULL THEN
            SELECT INTO tmp product_name FROM "Product" WHERE product_name = NEW.product_name;
        IF NOT FOUND THEN RAISE EXCEPTION 'my_fk_constraint';   
        END IF;
        END IF;
        RETURN NEW;
    END;
$prod_fk$ LANGUAGE plpgsql;

CREATE TRIGGER prod_fk
BEFORE INSERT OR UPDATE ON "Order/product"
    FOR EACH ROW EXECUTE PROCEDURE prod_fk();


--�������������� ���������� ��������� (�����������) ����(-��) 
--(�������� INSERT/UPDATE/DELETE)

CREATE TABLE "Point_delivery/phone"
(
  phone_number text NOT NULL,
  point_name text,
  PRIMARY KEY (phone_number),
  FOREIGN KEY (point_name)
      REFERENCES "Point_delivery" (point_name) 
);


CREATE OR REPLACE FUNCTION point_del() RETURNS TRIGGER AS $point_del$
DECLARE
    p_name text;
    ph_number text;
BEGIN
    IF    TG_OP = 'INSERT' THEN
        p_name = NEW.point_name ;
	ph_number := '0987654320';
        INSERT INTO "Point_delivery/phone"(phone_number, point_name) values (ph_number, p_name);
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
        p_name = NEW.point_name ;
	ph_number := '0987654320';
        INSERT INTO "Point_delivery/phone"(phone_number, point_name) values (ph_number, p_name);
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
        p_name = OLD.point_name ;
	ph_number := '0987654320';
        INSERT INTO "Point_delivery/phone"(phone_number, point_name) values (ph_number, p_name);
        RETURN OLD;
    END IF;
END;
$point_del$ LANGUAGE plpgsql;

CREATE TRIGGER point_del
AFTER INSERT OR UPDATE OR DELETE ON "Point_delivery" FOR EACH ROW EXECUTE PROCEDURE point_del();



