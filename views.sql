-- �������� ������������� customer_order ������� ����� �������� 
-- ������ �� 2 ������ "Customer" � "Order"
-- ��� �������, ��� ���������� ���������� �������, ���������� �� 
-- ������ 400 �.�.

CREATE VIEW customer_orders
          AS SELECT  a.login, a.first_name, a.middle_name, a.last_name, a.amount, b.order_number, b.order_status, b.order_value, b.way_to_get
             FROM "Customer" a, "Order" b
             WHERE a.amount>=400;

--������� ����������� �������:

GRANT SELECT ON customer_orders TO BD_administrator, Sales_manager, Site_administrator;

-- ��������� ��� ������������� �������� INSERT/UPDATE/DELETE

--Insert

CREATE RULE customer_orders_ins AS ON INSERT TO customer_orders
        DO INSTEAD (
        INSERT INTO "Customer" (login, first_name, middle_name, last_name, amount)
        VALUES (NEW.login, NEW.first_name, NEW.middle_name, NEW.last_name, NEW.amount);                    

        INSERT INTO "Order" (order_number, order_status, order_value, way_to_get)
        VALUES (NEW.order_number, NEW.order_status, NEW.order_value, NEW.way_to_get)
        );

--Update

CREATE RULE customer_orders_upd AS ON UPDATE TO customer_orders
        DO INSTEAD (
        UPDATE "Customer" SET 
                login= NEW.login,first_name=NEW.first_name, middle_name=NEW.middle_name, last_name=NEW.last_name, amount=NEW.amount
                          
	WHERE 
		login=OLD.login;
		
        UPDATE "Order" SET
                order_number=NEW.order_number, order_status=NEW.order_status, order_value=NEW.order_value, way_to_get=NEW.way_to_get
        WHERE order_number=OLD.order_number;
       
        );

--Delete

CREATE OR REPLACE RULE customer_orders_del AS ON DELETE TO customer_orders
        DO INSTEAD (
        DELETE FROM "Customer" WHERE login=OLD.login; 
        DELETE FROM "Order" WHERE order_number=OLD.order_number;
        );



--- CREATE TRIGGER


CREATE OR REPLACE FUNCTION customer_orders_func()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $function$
   BEGIN
      IF TG_OP = 'INSERT' THEN
        INSERT INTO "Customer" (login, first_name, middle_name, last_name, amount)
        VALUES (NEW.login, NEW.first_name, NEW.middle_name, NEW.last_name, NEW.amount);
        INSERT INTO "Order" (order_number, order_status, order_value, way_to_get)
        VALUES (NEW.order_number, NEW.order_status, NEW.order_value, NEW.way_to_get);
        RETURN NEW;
      ELSIF TG_OP = 'UPDATE' THEN
        UPDATE "Customer" SET
                login= NEW.login,first_name=NEW.first_name, middle_name=NEW.middle_name, last_name=NEW.last_name, amount=NEW.amount
                          
	WHERE 
		login=OLD.login;
		
        UPDATE "Order" SET
                order_number=NEW.order_number, order_status=NEW.order_status, order_value=NEW.order_value, way_to_get=NEW.way_to_get
        WHERE   order_number=OLD.order_number;
       
                RETURN NEW;
      ELSIF TG_OP = 'DELETE' THEN
                DELETE FROM "Customer" WHERE login=OLD.login; 
		DELETE FROM "Order" WHERE order_number=OLD.order_number;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$function$;

CREATE TRIGGER customer_orders_trigger
    INSTEAD OF INSERT OR UPDATE OR DELETE ON
      customer_orders FOR EACH ROW EXECUTE PROCEDURE customer_orders_func();


