CREATE TABLE "Customer"
(
  login text NOT NULL,
  password text NOT NULL,
  email text NOT NULL,
  first_name text NOT NULL,
  middle_name text,
  last_name text NOT NULL,
  adress text,
  birthday date,
  discount numeric(4,2),
  amount numeric(10,2),
  PRIMARY KEY (login),
  CHECK (login ~* '^[A-Za-z0-9]+$'),
  CHECK (discount >= 0 AND discount <= 100),
  CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'),
  CHECK (first_name ~* '^[A-Za-z-]+$'),
  CHECK (last_name ~* '^[A-Za-z-]+$'),
  CHECK (middle_name ~* '^[A-Za-z]+$')
);


CREATE TABLE "Credit_card"
(
  credit_card_number text NOT NULL,
  expiry_date date NOT NULL,
  login text NOT NULL,
  verification_code integer,
  PRIMARY KEY (credit_card_number),
  FOREIGN KEY (login) 
  REFERENCES "Customer" (login),
  CHECK (credit_card_number ~* '^[0-9]+$'),
  CHECK (verification_code >= 0 AND verification_code <= 999)
);


CREATE TABLE "Customer/phone_number"
(
  phone_number text NOT NULL,
  login text NOT NULL,
  PRIMARY KEY (phone_number),
  FOREIGN KEY (login) 
	REFERENCES "Customer" (login) 
);

CREATE TYPE valid_post AS ENUM ('Sales manager', 'Site administrator', 'Delivery manager','DB administrator', 'Courier');
CREATE TABLE "Employee"
(
  personnel_number integer NOT NULL,
  first_name text NOT NULL,
  middle_name text,
  last_name text NOT NULL,
  email text NOT NULL,
  post valid_post,
  phone_number text,
  PRIMARY KEY (personnel_number),
  CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'),
  CHECK (first_name ~* '^[A-Za-z-]+$'),
  CHECK (last_name ~* '^[A-Za-z-]+$'),
  CHECK (middle_name ~* '^[A-Za-z-]+$')
);

CREATE TABLE "Point_delivery"
(
  point_name text NOT NULL,
  address_point text NOT NULL,
  working_time text,
  PRIMARY KEY (point_name)
);


CREATE TABLE "Point_delivery/phone"
(
  phone_number text NOT NULL,
  point_name text,
  PRIMARY KEY (phone_number),
  FOREIGN KEY (point_name)
      REFERENCES "Point_delivery" (point_name) 
);


CREATE TABLE "Product"
(
  product_number integer NOT NULL,
  price numeric(10,2) NOT NULL,
  product_name text NOT NULL,
  category_name text NOT NULL,
  subcategory_name text NOT NULL,
  PRIMARY KEY (product_name),
  CHECK (category_name ~* '^[A-Za-z -]+$'),
  CHECK (product_name ~* '^[A-Za-z -]+$'),
  CHECK (subcategory_name ~* '^[A-Za-z -]+$')
);


CREATE TYPE valid_order_status AS ENUM ('fulfilled', 'not fulfilled', 'canceled');
CREATE TYPE valid_way_to_get AS ENUM ('conveyance', 'pickup');
CREATE TYPE valid_payment_method  AS ENUM ('credit card', 'ready money');
CREATE TABLE "Order"
(
  order_number integer NOT NULL,
  order_status valid_order_status,
  order_value numeric(10,2) DEFAULT 0,
  payment_method valid_payment_method,
  way_to_get valid_way_to_get,
  login text NOT NULL,
  personnel_number integer,
  point_name text,
  data_order timestamp without time zone,
  PRIMARY KEY (order_number),
  FOREIGN KEY (login)
	REFERENCES "Customer" (login),
  FOREIGN KEY (personnel_number)
      REFERENCES "Employee" (personnel_number),
  FOREIGN KEY (point_name)
      REFERENCES "Point_delivery" (point_name)
);

CREATE TABLE "Supplier"
(
  supplier_name text NOT NULL,
  email text NOT NULL,
  phone_number text NOT NULL,
  PRIMARY KEY (supplier_name),
  CHECK (email ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$'),
  CHECK (supplier_name ~* '^[A-Za-z0-9_ -]+$')
);

CREATE TABLE "Supplier/category"
(
  product_category text NOT NULL,
  supplier_name text NOT NULL,
  PRIMARY KEY (product_category),
  FOREIGN KEY (supplier_name)
      REFERENCES "Supplier" (supplier_name),
  CHECK (product_category ~* '^[A-Za-z_ -]+$')
);

CREATE TYPE valid_delivery_type AS ENUM ('courier', 'automobile transport', 'feight transport');
CREATE TYPE valid_delivery_status AS ENUM ('delivered', 'not delivered', 'canceled');
CREATE TABLE "Delivery"
(
  order_number integer NOT NULL,
  delivery_status valid_delivery_status NOT NULL,
  delivery_type valid_delivery_type,
  supplier_name text,
  personnel_number integer NOT NULL, 
  delivery_data timestamp without time zone,
  delivery_cost numeric(10,2),
  PRIMARY KEY (order_number),
  FOREIGN KEY (order_number) 
	REFERENCES "Order" (order_number),
  FOREIGN KEY (personnel_number) 
	REFERENCES "Employee" (personnel_number),
  FOREIGN KEY (supplier_name)REFERENCES "Supplier" (supplier_name)
);


CREATE TABLE "Order/product"
(
  order_number integer NOT NULL,
  product_name text NOT NULL,
  PRIMARY KEY (order_number),
  FOREIGN KEY (order_number)
      REFERENCES "Order" (order_number),
  FOREIGN KEY (product_name)
      REFERENCES "Product" (product_name)
);



