INSERT INTO "Customer" (login, password, email, first_name, middle_name, last_name, adress, birthday, discount, amount)
VALUES	('customer1', 'jok4','nikita@mail.ru', 'Nickolay','Ivanovich','Smirnov', 'Kiev...', '1968-09-12', '1', '3000'),
	('customer2', 'fgh6','natali-push@mail.ru', 'Natalia','Petrovna','Ivanova', 'Kiev...', '1988-04-05','3', '5000'),
	('customer3', '65ggfhy','vasya_pupkin@mail.ru', 'Vasiliy','Sergeevich','Petrov', 'Kiev...', '1985-03-14','2','4000');

INSERT INTO "Credit_card" (credit_card_number, expiry_date, login, verification_code )
VALUES	('1234987654907653', '2017-07-23', 'customer1', '234'),
	('1859047894367893', '2013-09-12', 'customer2', '786'),
	('16785940382759047', '2014-07-09', 'customer3', '876');

INSERT INTO "Customer/phone_number" (phone_number, login )
VALUES	('0998745678', 'customer1'),
	('0934576890', 'customer3'),
	('0667890561', 'customer2');

INSERT INTO "Employee" (personnel_number , first_name, middle_name, last_name, email, post, phone_number)
VALUES	('311017', 'Vladimir', 'Ivanovich', 'Ushakov', 'ushakov32@ukr.net', 'Sales maneger', '0956678904'),
	('345567', 'Irina', 'Vladimirovna', 'Bilas', 'bilas2@ukr.net', 'Site administrator', '0936678984'),
	('481210', 'Maria', 'Petrovna', 'Leonova', 'leonova64@ukr.net', 'Delivery manager', '0996878974'),
	('564789', 'Vladimir', 'Vladimirovich', 'Lermontov', 'lermontov_vlad@ukr.net', 'DB administrator', '09666789567'),
	('786540', 'Ivan', 'Ivanovich', 'Zabara', 'ivan_zabara32@ukr.net', 'Curier', '0666678544');

INSERT INTO "Point_delivery" (point_name, address_point, working_time)
VALUES('Obolon', 'Kiev subway exit to Dream Town', '9.00 - 21.00');

INSERT INTO "Point_delivery/phone" (phone_number, point_name)
VALUES ('0996754345', 'Obolon');

INSERT INTO "Product" (product_number, price, product_name, category_name, subcategory_name)
VALUES	('1','359','smoothing iron', 'household appliance', 'household'),
	('2','700','vacuum cleaner', 'household appliance', 'household'),
	('3','2499','washing mashine', 'household appliance', 'household'),
	('4','3899','cold storage', 'household appliance', 'kitchen appliance'),
	('5','2359','freezer', 'household appliance', 'kitchen appliance'),
	('6','9703','kitchen machine', 'household appliance', 'kitchen appliance'),
	('7','1599','blender', 'household appliance', 'small kitchen appliance'),
	('8','539','mixer', 'household appliance', 'small kitchen appliance'),
	('9','239', 'teapot', 'household appliance', 'small kitchen appliance');


INSERT INTO "Order" (order_number, order_status, order_value, payment_method, way_to_get, login, personnel_number, point_name, data_order)
VALUES	('1','fulfilled', '6700', 'credit card', 'conveyance','customer1','345567', 'Obolon', '2013-02-18'),
	('2','not fulfilled', '5600', 'ready money', 'pickup','customer2','481210', 'Obolon', '2013-02-23'),
	('3','canceled', '1500', 'ready money', 'pickup','customer3','311017', 'Obolon','2013-03-04');

INSERT INTO "Supplier" (supplier_name, email, phone_number)
VALUES	('TERRASOFT','terrasoft@gmail.ru','0987890567');

INSERT INTO "Supplier/category" (product_category, supplier_name)
VALUES	('household appliance','TERRASOFT');

INSERT INTO "Delivery" (order_number, delivery_status, delivery_type, supplier_name, personnel_number, delivery_data, delivery_cost)
VALUES	('1', 'delivered', 'automobile transport', 'TERRASOFT','311017','2013-03-06','50'),
	('2','not delivered', 'automobile transport', 'TERRASOFT','345567','2013-03-09', '75'),
	('3','canceled', 'curier', 'TERRASOFT','481210','2013-07-04', '50');


INSERT INTO "Order/product" (order_number, product_name)
VALUES	('3', 'smoothing iron'),
	('2', 'vacuum cleaner'),
	('1', 'washing mashine');




